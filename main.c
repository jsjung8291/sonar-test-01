#include <stdio.h>
#include <stdlib.h>

int test_func(int i) {
    if (i < 10) {
        printf("sonar coverage test1\n");
    }
    else if (i < 20) {
        printf("sonar coverage test2\n");
    }
    else if (i < 100) {
        printf("sonar coverage test3\n");
    }
    else {
        printf("sonar coverage test4\n");
    }

    return i;
}


int main()
{
    printf("sonar test start.\n");
    printf("sonar test 1.\n");
    printf("sonar test 2.\n");
    printf("sonar test 3.\n");
    printf("sonar test 4.\n");
    printf("sonar test 5.\n");
    test_func(10);
    test_func(1000);
    printf("sonar test end.\n");
}
